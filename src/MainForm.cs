﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.TextEditor;

namespace XmlPathExtractor
{
    public partial class frmMainForm : Form
    {
        XmlParser parser = new XmlParser();

        public frmMainForm()
        {
            InitializeComponent();
        }

        private void frmMainForm_Load(object senderObject, EventArgs ev)
        {
            Editor.TextChanged += (s, e) => parser.Load(Editor.Text);
            Editor.ActiveTextAreaControl.Caret.PositionChanged += (s, e) => renderPathInXml((s as Caret).Offset);
            loadFile(Environment.GetCommandLineArgs().Skip(1).FirstOrDefault(x => File.Exists(x)));
        }

        private void Caret_PositionChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void renderPathInXml(int position)
        {
            new Thread(() =>
            {
                setXmlPathText("Thinking...");
                try
                {
                    IEnumerable<string> nodes = parser.GetPathAtPosition(position).ToList();
                    if (!nodes.Any())
                    {
                        setXmlPathText(string.Empty);
                        return;
                    }
                    nodes = nodes.Reverse();
                    string padding = "  ";
                    string result = nodes.First();
                    nodes = nodes.Skip(1);
                    int nodeNumber = 1;
                    foreach (var node in nodes)
                    {
                        result += Environment.NewLine + string.Concat(Enumerable.Repeat(padding, nodeNumber++)) + node;
                    }
                    setXmlPathText(result);
                }
                catch
                {
                    setXmlPathText("Error");
                }
               
            }).Start();
        }

        private void setXmlPathText(string value)
        {
            txtXmlPath.Invoke((MethodInvoker)delegate
            {
                txtXmlPath.Text = value;
                txtXmlPath.AppendText(" ");
            });
        }
        
        private void loadFile(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            try
            {
                Editor.Text = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                loadFile(dialog.FileName);
            }
        }

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
            => System.Diagnostics.Process.Start("https://www.linkedin.com/in/bchojnacki/");
    }
}
