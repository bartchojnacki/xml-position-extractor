﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlPathExtractor.Parser
{
    static class Chopper
    {
        public static void ChopToStartOfComment(ref string content)
        {
            content = content.Trim();
            content = content.Substring(0, content.LastIndexOf("<!--")).Trim();
        }

        public static string ChopLastNodeOff(ref string content)
        {
            content = content.Trim();
            int lastNodeStart = content.LastIndexOf('<');
            string result = content.Substring(lastNodeStart);
            content = content.Substring(0, lastNodeStart).Trim();
            return result.Trim();
        }

        public static void ChopLastNodeByClosingTag(ref string content)
        {
            content = content.Trim();
            // <tag/>
            string tag = content.Substring(content.LastIndexOf("<"));
            tag = tag.Replace("</", string.Empty).Replace(">", string.Empty).Trim();
            string tagOpener = "<" + tag;

            content = content.Substring(0, content.LastIndexOf(tagOpener)).Trim();
        }

        public static void ChopEverythingAfterLastTag(ref string content)
        {
            content = content.Substring(0, content.LastIndexOf(">") + 1);
        }
    }
}
