﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlPathExtractor.Parser
{
    enum NodeType
    {
        EndOfComment,

        ClosingTag,

        SingleLiner,

        OpenTag,

        BeginOfFile,

        TagValue,
    }
}
