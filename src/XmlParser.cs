﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlPathExtractor.Parser;

namespace XmlPathExtractor
{
    public class XmlParser
    {
        string content;

        public XmlParser()
        {
            content = string.Empty;
            //1 . remove comments
        }

        public void Load(string fileContent)
        {
            content = fileContent ?? string.Empty;
        }

        public List<string> GetPathAtPosition(int positionInFile)
        {
            if (string.IsNullOrWhiteSpace(content) || positionInFile == 0)
            {
                return new List<string>();
            }

            string remainingText = content.Substring(0, positionInFile);
            int tagOpenerPos = remainingText.LastIndexOf('<');
            if (tagOpenerPos > remainingText.LastIndexOf('>'))
            {
                remainingText = remainingText.Substring(0, tagOpenerPos);
            }

            List<string> parentNodes = new List<string> { GetCurrentNode(positionInFile) };           

            parentNodes.AddRange(GetParentNodes(remainingText));
            return parentNodes;
        }

        private string GetCurrentNode(int positionInFile)
        {
            string before = content.Substring(0, positionInFile);
            string after = content.Substring(positionInFile);

            if (before.LastIndexOf("<") < before.LastIndexOf(">"))
            {
                return string.Empty;
            }

            if (after.IndexOf("<") < after.IndexOf(">"))
            {
                return string.Empty;
            }

            return before.Substring(before.LastIndexOf("<")) + after.Substring(0, after.IndexOf(">") + 1);
        }



        List<string> GetParentNodes(string remainingContent)
        {
            List<string> result = new List<string>();
            NodeType? lastNodeType = null;
            int iteration = 0;
            while (lastNodeType == null || lastNodeType.Value != NodeType.BeginOfFile)
            {
                if (iteration++ > 10000000)
                {
                    break; // failsafe
                }


                lastNodeType = getPreviousNodeType(ref remainingContent);
                switch (lastNodeType)
                {
                    case NodeType.EndOfComment:
                        Chopper.ChopToStartOfComment(ref remainingContent);
                        continue;

                    case NodeType.SingleLiner:
                        Chopper.ChopLastNodeOff(ref remainingContent);
                        continue;

                    case NodeType.OpenTag:
                        result.Add(Chopper.ChopLastNodeOff(ref remainingContent));
                        continue;

                    case NodeType.ClosingTag:
                        Chopper.ChopLastNodeByClosingTag(ref remainingContent);
                        continue;

                    case NodeType.TagValue:
                        Chopper.ChopEverythingAfterLastTag(ref remainingContent);
                        continue;
                }
            }

            return result;
        }

        NodeType getPreviousNodeType(ref string content)
        {
            content = content.Trim();
            if (content.EndsWith("-->"))
            {
                return NodeType.EndOfComment;
            }

            if (content.EndsWith("/>"))
            {
                return NodeType.SingleLiner;
            }

            if (content.EndsWith(">") && content.LastIndexOf('<') == content.LastIndexOf("</"))
            {
                return NodeType.ClosingTag;
            }

            if (content.EndsWith(">"))
            {
                return NodeType.OpenTag;
            }

            if (!content.Contains('>'))
            {
                return NodeType.BeginOfFile;
            }

            return NodeType.TagValue;

        }
        
    }
}
